<?php
require __DIR__.'/../core/active_record.php';
class TestRecord extends BaseActiveRecord {
    protected static $tablename = 'test_results';
    protected static $dbfields = array();

    public $id = 0;
    public $date = '';
    public $name = '';
    public $ugroup = '';
    public $ans_1_1 = '';
    public $ans_1_2 = '';
    public $ans_2 = '';
    public $ans_3 = '';
    public $correct = 0;
    
    public function __construct() 
    {
        parent::__construct();
    }
} 