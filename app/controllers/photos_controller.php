<?php
require __DIR__."/../core/controller.php";
class PhotosController extends Controller
{
    function index() 
    {
        $this->view->render("photos/index.php", "Фотоальбом", $this->model);
    }
}