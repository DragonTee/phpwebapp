<?php
require __DIR__."/../core/controller.php";
require __DIR__."/../entities/test.php";
class TestController extends Controller
{
    function index() 
    {
        if ($_SERVER["REQUEST_METHOD"] == "POST")
        {
            if (!$this->model->isErrorSet('name') && !$this->model->isErrorSet('group'))
            {
                $this->addNewResult();
            }
        }
        $this->loadResults();
        $this->view->render("test/index.php", "Тест", $this->model);
    }

    function loadResults()
    {
        $testRecord = new TestRecord();
        $records = $testRecord->findall();
        if ($records != null)
        {
            foreach ($records as $record) {
                array_push($this->model->testResults, $record);
            }
            usort($this->model->testResults, function ($a, $b) {
                return strcmp($a->date, $b->date);
            });
        }
    }

    function addNewResult()
    {
        $model = $this->model;
        $record = new TestRecord();
        $record->name = $model->name;
        $record->ugroup = $model->group;
        $record->correct = 3 - $model->countErrors();
        $record->date = date("Y/m/d h:i:sa");
        $record->ans_1_1 = $model->ans_1_1;
        $record->ans_1_2 = $model->ans_1_2;
        $record->ans_2 = $model->ans_2;
        $record->ans_3 = $model->ans_3;
        $record->save();
    }
}