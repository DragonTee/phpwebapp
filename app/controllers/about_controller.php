<?php
require __DIR__."/../core/controller.php";
class AboutController extends Controller
{
    function index() 
    {
        $this->view->render("about/index.php", "Обо мне", $this->model);
    }
}