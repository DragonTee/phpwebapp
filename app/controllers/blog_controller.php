<?php
require __DIR__."/../core/controller.php";
require __DIR__."/../entities/blog.php";
class BlogController extends Controller
{
    protected static $imagesPath = __DIR__.'../../stored/images/';
    function index() 
    {
        $this->loadBlog();
        $this->view->render("blog/index.php", "Мой блог", $this->model);
    }
    
    function loadBlog() 
    {
        $blogRecord = new BlogRecord();
        $page = 0;
        $pageSize = 5;
        if (isset($_GET['page']) && isset($_GET['page_size']))
        {
            $page = $_GET['page'];
            $pageSize = $_GET['page_size'];
            $records = $blogRecord->findPage($pageSize,$page);
        }
        else 
        {
            $records = $blogRecord->findPage(5,0);
        }
        
        if ($records != null)
        {
            foreach ($records as $record) {
                array_push($this->model->blogs, $record);
            }
            usort($this->model->blogs, function ($a, $b) {
                return strcmp($a->date, $b->date);
            });
            $this->model->page = $page;
            $this->model->pageSize = $pageSize;
            $this->model->maxPages = intdiv($blogRecord->count() - 1, $pageSize) + 1;
        }
    }
}