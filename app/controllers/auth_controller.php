<?php
require __DIR__."/../core/controller.php";
require __DIR__."/../entities/user.php";
class AuthController extends Controller
{
    protected static $imagesPath = __DIR__.'../../stored/images/';
    function index() 
    {
        if ($_SERVER["REQUEST_METHOD"] == "POST")
        {
            $this->login();
        }
        else 
        {
            unset($_SESSION['LastError']);
        }
        $this->view->render("auth/index.php", "Авторизация пользователя", $this->model);
    }
    
    function register() 
    {
        if ($_SERVER["REQUEST_METHOD"] == "POST")
        {
            if ($this->model->countErrors() == 0)
            {
                header('Location:/auth/index');
                $this->createUser();
                exit;
            }
        }
        $this->view->render("auth/register.php", "Регистрация пользователя", $this->model);
    }

    function logout()
    {
        if (isset($_SESSION['Logged']))
        {
            unset($_SESSION['Logged']);
            unset($_SESSION['FIO']);
        }
        session_destroy();
        header('Location:/auth/index');
        exit;
    }

    function login()
    {
        if (isset($_SESSION['Logged']))
        {
            header('Location:/home/index');
            exit;
        }
        $user = new UserRecord();
        $user = UserRecord::findByLogin($this->model->login);
        if ($user != NULL)
        {
            if (md5($this->model->password) == $user->password)
            {
                $_SESSION['Logged'] = 1;
                $_SESSION['FIO'] = $user->fio;
                unset($_SESSION['LastError']);
                header('Location:/home/index');
                exit;
            }
            else 
            {
                $_SESSION['LastError'] = 'WrongPass';
                return;
            }
        }
        else
        {
            $_SESSION['LastError'] = 'WrongUser';
            return;
        }
    }

    function createUser()
    {
        $model = $this->model;
        $user = new UserRecord();
        $otherUser = UserRecord::findByLogin($model->login);
        if ($otherUser != NULL)
        {
            header('Location:/auth/register');
            header('Error:UserExists');
            exit;
        }
        $user->fio = $model->fio;
        $user->password = md5($model->password);
        $user->email = $model->email;
        $user->login = $model->login;
        $user->save();
    }
}