<?php
require __DIR__."/../core/controller.php";
class HomeController extends Controller
{
    function index() 
    {
        $this->view->render("home/index.php", "Главная", $this->model);
    }
}