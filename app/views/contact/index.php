<script src="/app/public/assets/js/calendar-script.js"></script>
<section class="container">
  <h1 class="centered">
    Контакт
  </h1>
  <div class="contact">
    <form id="contact-form" method="POST" onsubmit="return checkContactForm(this)">
      <label for="name">ФИО</label>
      <div class="popover">
        <div class="popover-content">Поле ФИО должно содержать 3 слова, разделенных пробелами</div>
      </div>
      <?php echo '<input name="name" onmouseenter="showPopover(this)" onmouseleave="hidePopover(this)" placeholder="ФИО" type="text" value="'.$model->name.'" >'?>
      <label class="error-block" for="name" id="name-error">
        <?php echo $model->showError('name'); ?>
      </label>
      <label for="phone">Телефон</label>
      <div class="popover">
        <div class="popover-content">Поле телефон должно начинаться с +7 или +3, не должно содержать пробелов и должно содержать от 9 до 11 цифр</div>
      </div>
      <?php echo '<input name="phone" onmouseenter="showPopover(this)" onmouseleave="hidePopover(this)" placeholder="Телефон" type="text" value="'.$model->phone.'" >'?>
      <label class="error-block" for="phone" id="phone-error">
        <?php echo $model->showError('phone'); ?>
      </label>
      <label>Пол</label>
      <div class="gender-select">
        <input checked="checked" id="gender_male" name="gender" type="radio" value="male">
        <label for="gender_male">Мужской</label>
        <input id="gender_female" name="gender" type="radio" value="female">
        <label for="gender_female">Женский</label>
      </div>
      <label for="birthdate">Дата рождения</label>
      <div class="popover">
        <div class="popover-content">Дата должна быть введена в формате ММ/ЧЧ/ГГГГ</div>
      </div>
      <?php echo '<input autocomplete="off" class="calendar-field" name="birthdate" onfocus="calendarDropdown(event)" onmouseenter="showPopover(this)" onmouseleave="hidePopover(this)" placeholder="" type="text" value="'.$model->birthdate.'" >'?>
      <div class="dropdown-content calendar-field" id="calendarDropdown">
        <div class="month-selector calendar-field">
          <span class="calendar-field">
            <select class="calendar-field" id="calendar-month" name="groups" onchange="updateDays();">
              <option class="calendar-field" value="0">January</option>
              <option class="calendar-field" value="1">February</option>
              <option class="calendar-field" value="2">March</option>
              <option class="calendar-field" value="3">April</option>
              <option class="calendar-field" value="4">May</option>
              <option class="calendar-field" value="5">June</option>
              <option class="calendar-field" value="6">July</option>
              <option class="calendar-field" value="7">August</option>
              <option class="calendar-field" value="8">September</option>
              <option class="calendar-field" value="9">October</option>
              <option class="calendar-field" value="10">November</option>
              <option class="calendar-field" value="11">December</option>
            </select>
          </span>
          <span class="calendar-field">
            <input class="calendar-field" id="calendar-year" max="2020" min="1900" onchange="updateDays();" type="number" value="2000">
          </span>
        </div>
        <div class="calendar-field calendar-table">
          <script>
            initCalendar();
          </script>
        </div>
      </div>
      <label class="error-block" for="birthdate" id="date-error">
        <?php echo $model->showError('birthdate'); ?>
      </label>
      <label for="email">Электронная почта</label>
      <div class="popover">
        <div class="popover-content">Email должен быть введен в формате example@mail.com</div>
      </div>
      <?php echo '<input name="email" onmouseenter="showPopover(this)" onmouseleave="hidePopover(this)" placeholder="Электронная почта" type="email" value="'.$model->email.'" >'?>
      <label class="error-block" for="email" id="email-error">
        <?php echo $model->showError('email'); ?>
      </label>
      <label for="message">Сообщение</label>
      <div class="popover">
       <div class="popover-content">Сообщение должно быть непустым</div>
      </div>
      <textarea name="message" name="message" onmouseenter="showPopover(this)" onmouseleave="hidePopover(this)" placeholder="Сообщение"><?php echo $model->message; ?></textarea>
      <label class="error-block" for="message" id="message-error">
        <?php echo $model->showError('message'); ?>
      </label>
      <div class="submit-container">
        <input class="submit" id="submit-button" onclick="showModalPrompt();" value="Отправить">
        <input class="submit" onclick="clearAllFields();" type="reset" value="Очистить форму ">
      </div>
    </form>
  </div>
</section>
<div class="submit-container">
  <div class="modal-window" id="modal-prompt">
    <div class="submit-container modal-window-content">
      <div>Вы действительно хотите отправить сообщение?</div>
      <input class="submit" id="modal-no-btn" onclick="hideModalPrompt();" value="Нет">
      <input class="submit" id="modal-yes-btn" onclick="submitForm();" type="submit" value="Да ">
    </div>
  </div>
</div>