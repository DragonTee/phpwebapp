<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1, shrink-to-fit=no'>
    <link href="/app/public/assets/css/styles.css" rel="stylesheet">
    <script src="/app/public/assets/js/navbar-script.js"></script>
    <script src="/app/public/assets/js/jquery-3.6.0.js"></script>
    <title><?=$title?></title>
    <link rel="icon" type="image/x-icon" href="/app/public/assets/img/favicon.png">
</head>
<body>
    <header>
        <nav>
            <ul>  
                <?php if (isset($_SESSION['FIO']))
                {
                    echo '<div class="centered">';
                    echo 'Пользователь: '.$_SESSION['FIO'];
                    echo '</div>';
                }
                ?>                
                <li>
                    <a href="<?php echo isset($_SESSION['Logged']) ? '/auth/logout' : '/auth/index'?>" style="margin-left:1rem"><?php echo isset($_SESSION['Logged']) ? 'Выход' : 'Вход'?></a>
                </li>
                <li onmouseenter="hoverNavbarIcon(event)" onmouseleave="leaveNavbarIcon(event)">
                    <img alt="photo" src="https://picsum.photos/id/130/100" style="width:2rem">
                    <a href="/home/">Главная</a>
                </li>
                    <li onmouseenter="hoverNavbarIcon(event)" onmouseleave="leaveNavbarIcon(event)">
                    <img alt="photo" src="https://picsum.photos/id/130/100" style="width:2rem">
                    <a href="/about/">Обо мне</a>
                </li>
                    <li onmouseenter="hoverNavbarIcon(event)" onmouseleave="leaveNavbarIcon(event)">
                    <img alt="photo" src="https://picsum.photos/id/130/100" style="width:2rem">
                    <a class="dropbtn" onclick="interestsDropdown(event)">Мои интересы</a>
                    <ul class="dropdown-content" id="interestsDropdown">
                        <li>
                            <a href="/interests/#hobby">Хобби</a>
                        </li>
                        <li>
                            <a href="/interests/#books">Книги</a>
                        </li>
                        <li>
                            <a href="/interests/#films">Фильмы</a>
                        </li>
                        <li>
                            <a href="/interests/#music">Музыка</a>
                        </li>
                    </ul>
                </li>
                <li onmouseenter="hoverNavbarIcon(event)" onmouseleave="leaveNavbarIcon(event)">
                    <img alt="photo" src="https://picsum.photos/id/130/100" style="width:2rem">
                    <a href="/study/">Учеба</a>
                </li>
                <li onmouseenter="hoverNavbarIcon(event)" onmouseleave="leaveNavbarIcon(event)">
                    <img alt="photo" src="https://picsum.photos/id/130/100" style="width:2rem">
                    <a href="/photos/">Фотоальбом</a>
                </li>
                <li onmouseenter="hoverNavbarIcon(event)" onmouseleave="leaveNavbarIcon(event)">
                    <img alt="photo" src="https://picsum.photos/id/130/100" style="width:2rem">
                    <a href="/contact/">Контакт</a>
                </li>
                <li onmouseenter="hoverNavbarIcon(event)" onmouseleave="leaveNavbarIcon(event)">
                    <img alt="photo" src="https://picsum.photos/id/130/100" style="width:2rem">
                    <a href="/test/">Тест</a>
                </li>
                <li onmouseenter="hoverNavbarIcon(event)" onmouseleave="leaveNavbarIcon(event)">
                    <img alt="photo" src="https://picsum.photos/id/130/100" style="width:2rem">
                    <a href="/guests/">Гостевая книга</a>
                </li>
                </li>
                <li onmouseenter="hoverNavbarIcon(event)" onmouseleave="leaveNavbarIcon(event)">
                    <img alt="photo" src="https://picsum.photos/id/130/100" style="width:2rem">
                    <a href="/blog/">Мой блог</a>
                </li>
                <div class="centered" id="clockText"></div>
            </ul>
        </nav>
    </header>
    <?php
        include 'app/views/'.$content_view;
    ?>
</body> 
</html>