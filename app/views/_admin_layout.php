<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1, shrink-to-fit=no'>
    <link href="/app/public/assets/css/styles.css" rel="stylesheet">
    <script src="/app/public/assets/js/navbar-script.js"></script>
    <script src="/app/public/assets/js/jquery-3.6.0.js"></script>
    <title><?=$title?></title>
    <link rel="icon" type="image/x-icon" href="/app/public/assets/img/favicon.png">
</head>
<body>
    <header>
        <nav>
            <ul>
                <div class="centered">Администратор</div>
                <li onmouseenter="hoverNavbarIcon(event)" onmouseleave="leaveNavbarIcon(event)">
                    <img alt="photo" src="https://picsum.photos/id/130/100" style="width:2rem">
                    <a href="/admin/home/">Главная</a>
                </li>
                </li>
                    <li onmouseenter="hoverNavbarIcon(event)" onmouseleave="leaveNavbarIcon(event)">
                    <img alt="photo" src="https://picsum.photos/id/130/100" style="width:2rem">
                    <a class="dropbtn" onclick="guestsDropdown(event)">Гостевая книга</a>
                    <ul class="dropdown-content" id="guestsDropdown">
                        <li>
                            <a href="/admin/guests/">Гостевая книга</a>
                        </li>
                        <li>
                            <a href="/admin/guests/add">Загрузка сообщений гостевой книги</a>
                        </li>
                    </ul>
                </li>
                </li>
                    <li onmouseenter="hoverNavbarIcon(event)" onmouseleave="leaveNavbarIcon(event)">
                    <img alt="photo" src="https://picsum.photos/id/130/100" style="width:2rem">
                    <a class="dropbtn" onclick="blogDropdown(event)">Блог</a>
                    <ul class="dropdown-content" id="blogDropdown">
                        <li>
                            <a href="/admin/blog/add">Редактор блога</a>
                        </li>
                        <li>
                            <a href="/admin/blog/">Мой блог</a>
                        </li>
                        <li>
                            <a href="/admin/blog/load">Загрузка сообщений блога</a>
                        </li>
                    </ul>
                </li>
                <li onmouseenter="hoverNavbarIcon(event)" onmouseleave="leaveNavbarIcon(event)">
                    <img alt="photo" src="https://picsum.photos/id/130/100" style="width:2rem">
                    <a href="/admin/stats/">Статистика посещений</a>
                </li>
                <div class="centered" id="clockText"></div>
            </ul>
        </nav>
    </header>
    <?php
        include 'app/admin/views/'.$content_view;
    ?>
</body> 
</html>