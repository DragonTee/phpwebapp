<script src="/app/public/assets/js/calendar-script.js"></script>
<section class="container">
    <h1 class="centered">
        Вопросы по дисциплине
    </h1>
    <h1 class="centered">
        Основы программирования и алгоритмические языки
    </h1>
    <div class="contact">
        <form name="questionForm" method="POST">
            <label for="name">ФИО</label>
            <?php echo '<input name="name" placeholder="ФИО" type="text" value="'.$model->name.'" >'?>
            <label class="error-block" for="name" id="name-error">
                <?php echo $model->showError('name'); ?>
            </label>
            <label for="group-select">Группа</label>
            <select name="group" id="group-select">
                <?php 
                if (!empty($model->group)) {
                    echo '<option value="'.$model->group.'">'.$model->group.'</option>';
                }
                else {
                    echo '<option value="">--Выберите группу--</option>';
                }
                ?>
                <option value="IS-b-19-1">ИС/б-19-1-о</option>
                <option value="IS-b-19-2">ИС/б-19-2-о</option>
                <option value="PI-b-19-1">ПИ/б-19-1-о</option>
                <option value="IVT-b-19-1">ИВТ/б-19-1-о</option>
                <option value="IVT-b-19-2">ИВТ/б-19-2-о</option>
                <option value="PIN-b-19-1">ПИН/б-19-1-о</option>
                <option value="UTS-b-19-1">УТС/б-19-1-о</option>
                <option value="UTS-b-19-2">УТС/б-19-2-о</option>
            </select>
            <label class="error-block" for="group" id="group-error">
                <?php echo $model->showError('group'); ?>
            </label>
            <?php 
            if ($model->testResult('ans_1_1'))
                echo '<div class="question valid-input">';
            else 
                echo '<div class="question">';
            ?>
                <h2>Вопрос 1:</h2>
                <p>Выберите только Ответ 2</p>
                <div class="answers">
                    <?php
                    if (!empty($model->ans_1_1)) {
                        echo '<input type="checkbox" id="ans_1_1" name="ans_1_1" checked>';
                    }
                    else {
                        echo '<input type="checkbox" id="ans_1_1" name="ans_1_1">';
                    }
                    ?>
                    <label for="ans_1-1">Ответ 1</label>
                    <?php
                    if (!empty($model->ans_1_2)) {
                        echo '<input type="checkbox" id="ans_1_2" name="ans_1_2" checked>';
                    }
                    else {
                        echo '<input type="checkbox" id="ans_1_2" name="ans_1_2">';
                    }
                    ?>
                    <label for="ans_1-2">Ответ 2</label>
                </div>
                <label class="error-block" for="ans_1" id="ans_1-error">
                    <?php echo $model->showError('ans_1_1'); ?>
                </label>
            </div>
            <?php 
            if ($model->testResult('ans_2'))
                echo '<div class="question valid-input">';
            else 
                echo '<div class="question">';
            ?>
                <h2>Вопрос 2:</h2>
                <p>16 / 4 = ?</p>
                <div class="answers">
                    <label>Выберите вариант ответа:
                        <select name="ans_2">
                            <option value="">--Выберите Ответ--</option>
                        <optgroup label="Группа 1">
                            <option value="ans_2-1">Один</option>
                            <option value="ans_2-2">Два</option>
                            <option value="ans_2-3">Три</option>
                        </optgroup>
                        <optgroup label="Группа 2">
                            <option value="ans_2-4">Один</option>
                            <option value="ans_2-5">Два</option>
                            <option value="ans_2-6">Три</option>
                            <option value="ans_2-7">Четыре</option>
                        </optgroup>
                        </select>
                    </label>
                </div>
                <label class="error-block" for="ans_2" id="ans_2-error">
                    <?php echo $model->showError('ans_2'); ?>
                </label>
            </div>
            <?php 
            if ($model->testResult('ans_3'))
                echo '<div class="question valid-input">';
            else 
                echo '<div class="question">';
            ?>
                <h2>Вопрос 3:</h2>
                <p>Какую таблетку принял Нео в Матрице?</p>
                <p>a - Красную. b - Синюю.</p>
                <div class="answers">
                    <label for="ans_3">Ответ</label>
                    <?php echo '<input type="text" placeholder="Ответ" name="ans_3" value="'.$model->ans_3.'" >'?>
                </div>
                <label class="error-block" for="ans_3" id="ans_3-error">
                    <?php echo $model->showError('ans_3'); ?>
                </label>
            </div>
            <div class="submit-container">
                <input type="submit" value="Отправить" class="submit">
                <input type="reset" value="Очистить форму" class="submit">
            </div>
        </form>
    </div>
</section>

<br>

<?php 
if (isset($_SESSION['Logged']))
{
    foreach ($model->testResults as $result)
    {
        echo '<section class="container" style="max-width: 70%;">';
        echo '<div class="question">';
            echo '<br>';
            echo '<div class="centered">';
                echo 'Ответы '.$result->name;
            echo '</div>';
            echo '<div class="answers"> Вопрос 1: '.$result->ans_1_1.' '.$result->ans_1_2.'</div>';
            echo '<div class="answers"> Вопрос 2: '.$result->ans_2.'</div>';
            echo '<div class="answers"> Вопрос 3: '.$result->ans_3.'</div>';
            echo '<div class="answers"> Верных ответов: '.$result->correct.'/3</div>';
            echo '<div class="result-date">Группа: '.$result->ugroup.'</div>';
            echo '<div class="result-date">Дата: '.$result->date.'</div>';
        echo '</div>';
        echo '</section>';
        echo '<br>';
    }
}
?>