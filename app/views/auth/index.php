<section class="container">
    <h1 class="centered">
        Авторизация пользователя
    </h1>
    <div class="contact">
        <form name="loginForm" method="POST">
            <label for="login">Логин</label>
            <input name="login" placeholder="Логин" type="text" value="<?php echo $model->login ?>">
            <label class="error-block" for="login" id="login-error">
                <?php echo $model->showError('login'); ?>
                <?php
                    if ($_SESSION['LastError'] == 'WrongUser')
                    {
                        echo "Пользователя с таким логином не существует";
                    }
                ?>
            </label>
            <label for="password">Пароль</label>
            <input name="password" placeholder="Пароль" type="password" value="<?php echo $model->password ?>">
            <label class="error-block" for="password" id="password-error">
                <?php echo $model->showError('password'); ?>
                <?php
                if ($_SESSION['LastError'] == 'WrongPass')
                {
                    echo "Неверный пароль";
                }
                ?>
            </label>
            <div class="submit-container">
                <input class="submit" type="submit" id="submit-button" value="Войти">
                <a href="/auth/register" class="submit">Регистрация</a>
            </div>                
        </form>
    </div>
</section>
