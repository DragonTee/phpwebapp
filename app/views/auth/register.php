<section class="container">
    <h1 class="centered">
        Регистрация пользователя
    </h1>
    <div class="contact">
        <form name="registerForm" method="POST">
            <label for="fio">ФИО</label>
            <input name="fio" placeholder="ФИО" type="text" value="<?php echo $model->fio ?>">
            <label class="error-block" for="fio" id="fio-error">
                <?php echo $model->showError('fio'); ?>
            </label>

            <label for="email">Электронная почта</label>
            <input name="email" placeholder="Электронная почта" type="email" value="<?php echo $model->email ?>">
            <label class="error-block" for="email" id="email-error">
                <?php echo $model->showError('email'); ?>
            </label>

            <label for="login">Логин</label>
            <input name="login" placeholder="Логин" type="text" value="<?php echo $model->login ?>">
            <label class="error-block" for="login" id="login-error">
                <?php echo $model->showError('login'); ?>
            </label>

            <label for="password">Пароль</label>
            <input name="password" placeholder="Пароль" type="password" value="<?php echo $model->password ?>">
            <label class="error-block" for="password" id="password-error">
                <?php echo $model->showError('password'); ?>
            </label>

            <div class="submit-container">
                <input class="submit" type="submit" id="submit-button" value="Регистрация">
            </div>
        </form>
    </div>
</section>
