<div class="container">
    <h1 class="centered">
        Мои интересы
    </h1>
    <section>
        <a id="hobby"></a>
        <h2>
        Моё хобби
        </h2>
        <p>
        Libero consequat condimentum posuere dui nulla a taciti tempor consectetur conubia aenean habitasse ut placerat consectetur fames. Per parturient erat odio condimentum parturient mattis scelerisque eu duis nascetur in fringilla justo ut scelerisque.
        </p>
        <img alt="photo" src="https://picsum.photos/id/1/500" style="width:25%">
        <img alt="photo" src="https://picsum.photos/id/2/500" style="width:25%">
    </section>
    <section>
        <a id="books"></a>
        <h2>
        Мои любимые книги
        </h2>
        <p>
        Libero consequat condimentum posuere dui nulla a taciti tempor consectetur conubia aenean habitasse ut placerat consectetur fames. Per parturient erat odio condimentum parturient mattis scelerisque eu duis nascetur in fringilla justo ut scelerisque.
        </p>
        <img alt="photo" src="https://picsum.photos/id/367/500" style="width:25%">
        <img alt="photo" src="https://picsum.photos/id/444/500" style="width:25%">
    </section>
    <section>
        <a id="films"></a>
        <h2>
        Мои любимые фильмы
        </h2>
        <p>
        Libero consequat condimentum posuere dui nulla a taciti tempor consectetur conubia aenean habitasse ut placerat consectetur fames. Per parturient erat odio condimentum parturient mattis scelerisque eu duis nascetur in fringilla justo ut scelerisque.
        </p>
        <img alt="photo" src="https://picsum.photos/id/354/500" style="width:25%">
        <img alt="photo" src="https://picsum.photos/id/420/500" style="width:25%">
    </section>
    <section>
        <a id="music"></a>
        <h2>
        Моя любимая музыка
        </h2>
        <p>
        Libero consequat condimentum posuere dui nulla a taciti tempor consectetur conubia aenean habitasse ut placerat consectetur fames. Per parturient erat odio condimentum parturient mattis scelerisque eu duis nascetur in fringilla justo ut scelerisque.
        </p>
        <img alt="photo" src="https://picsum.photos/id/452/500" style="width:25%">
        <img alt="photo" src="https://picsum.photos/id/453/500" style="width:25%">
    </section>
</div>