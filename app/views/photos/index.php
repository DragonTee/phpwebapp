<script src="/app/public/assets/js/photos-script.js"></script>
<div class="container">
    <h1 class="centered">
        Фотоальбом
    </h1>
        <div class="modal" id="modal-main">
        <span class="close" id="modal-close">×</span>
        <span id="modal-left" onclick="slideImage(-1)"><</span>
        <span id="modal-right" onclick="slideImage(1)">></span>
        <img class="modal-content" id="modal-image">
        <div id="modal-caption"></div>
    </div>
    <?php
    for ($i = 0; $i < 15; $i++)
    {
        if ($i % 3 == 0)
        {
            echo '<div class="row">';
        }
        echo '<div class="col">
        <img alt="'.$model->photo_names[$i].'" class="popupable" src="'.$model->photo_links[$i].'" title="'.$model->photo_names[$i].'">
        <p class="centered">'.$model->photo_names[$i].'</p>
        </div>';
        if ($i % 3 == 2)
        {
            echo '</div>';
        }
    }
    ?>
</div>