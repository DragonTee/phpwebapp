<?php
namespace Services
{
    class StatsService
    {
        private static function saveStats($page)
        {
            require 'app/entities/stats.php';
            $record = new StatsRecord();
            $record->saveStats($page);
        }
    }
}