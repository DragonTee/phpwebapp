<?php
require __DIR__.'/../core/model.php';
class ContactModel extends Model
{
    public $phone = '';
    public $name = '';
    public $email = '';
    public $birthdate =  '';
    public $gender = '';
    public $message = '';
    function __construct()
    {
        parent::__construct();
        
        if ($_SERVER["REQUEST_METHOD"] != "POST") return;

        $this->validator->SetRule('name', 'isNotEmpty');
        $this->validator->SetRule('name', 'isFio');
        $this->validator->SetRule('phone', 'isNotEmpty');
        $this->validator->SetRule('phone', 'isPhone');
        $this->validator->SetRule('birthdate', 'isNotEmpty');
        $this->validator->SetRule('birthdate', 'isDate');
        $this->validator->SetRule('email', 'isNotEmpty');
        $this->validator->SetRule('email', 'isEmail');
        $this->validator->SetRule('message', 'isNotEmpty');
        $this->validator->validate([
            'name' => $this->name,
            'phone' => $this->phone,
            'email' => $this->email,
            'birthdate' => $this->birthdate,
            'message' => $this->message,
        ]);
    }
}