<?php
require __DIR__.'/../core/model.php';
class AuthorizationModel extends Model
{
    public $email = '';
    public $password = '';
    function __construct()
    {
        parent::__construct();
        
        if ($_SERVER["REQUEST_METHOD"] != "POST") return;

        $this->validator->SetRule('email', 'isNotEmpty');
        $this->validator->SetRule('email', 'isEmail');
        $this->validator->SetRule('password', 'isNotEmpty');
        $this->validator->validate([
            'password' => $this->password,
            'email' => $this->email,
        ]);
    }
}