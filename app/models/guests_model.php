<?php
require __DIR__.'/../core/model.php';
class GuestsModel extends Model
{
    public $name = '';
    public $email = '';
    public $message = '';
    public $messages = '';
    function __construct()
    {
        parent::__construct();
        
        if ($_SERVER["REQUEST_METHOD"] != "POST") return;

        $this->validator->SetRule('name', 'isNotEmpty');
        $this->validator->SetRule('name', 'isFio');
        $this->validator->SetRule('email', 'isNotEmpty');
        $this->validator->SetRule('email', 'isEmail');
        $this->validator->SetRule('message', 'isNotEmpty');
        $this->validator->validate([
            'name' => $this->name,
            'email' => $this->email,
            'message' => $this->message,
        ]);
    }
}