<?php
require __DIR__.'/../core/model.php';
class PhotosModel extends Model
{
    public $photo_names;
    public $photo_links;
    function __construct()
    {
        parent::__construct();
        for ($i = 0; $i < 15; $i++)
        {
            $this->photo_names[$i] = 'Фото '.($i+1);        
            $this->photo_links[$i] = 'https://picsum.photos/id/'.($i+500).'/800';
        }
    }
}