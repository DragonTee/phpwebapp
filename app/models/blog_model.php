<?php
require __DIR__.'/../core/model.php';
class BlogModel extends Model
{
    public $blogs = [];
    public $date = '';
    public $author = '';
    public $subject = '';
    public $message = '';
    public $maxPages = 1;
    public $page = 0;
    public $pageSize = 5;
    function __construct()
    {
        parent::__construct();
        
        if ($_SERVER["REQUEST_METHOD"] != "POST") return;

        $this->validator->SetRule('author', 'isNotEmpty');
        $this->validator->SetRule('subject', 'isNotEmpty');
        $this->validator->SetRule('message', 'isNotEmpty');
        $this->validator->validate([
            'author' => $this->author,
            'subject' => $this->subject,
            'message' => $this->message,
        ]);
    }
}