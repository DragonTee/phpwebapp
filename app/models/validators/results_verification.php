<?php
class ResultsVerification extends CustomFormValidation 
{
    function __construct()
    {
        $this->messages['testAnswer1'] = 'Ответ неверный';
        $this->messages['testAnswer2'] = 'Ответ неверный';
        $this->messages['testAnswer3'] = 'Ответ неверный';
        $this->messages['isOneLetter'] = 'Поле должно содержать только 1 букву';
    }

    public function testAnswer1($ans1, $ans2) {
        return empty($ans1) && !empty($ans2);
    }

    public function testAnswer2($ans) {
        return $ans === 'ans_2-7';
    }

    public function testAnswer3($ans) {
        return $ans === 'a';
    }

    public function testResult($data) {
        return empty($this->errors[$data]);
    }
}