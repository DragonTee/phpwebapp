<?php
class CustomFormValidation extends FormValidation 
{
    function __construct()
    {
        $this->messages['isOneLetter'] = 'Поле должно содержать только 1 букву';
    }

    public function isOneLetter($data) {
        return ((strlen($data) == 1) && ctype_alpha($data));
    }
}