<?php
class FormValidation 
{
    public $rules = [];
    public $errors = [];
    public $messages = [
        'isNotEmpty' => 'Поле должно быть заполнено',
        'isInteger' => 'Поле не целое число',
        'isLess' => 'Поле больше заданного числа',
        'isPhone' => 'Неверный формат телефона',
        'isFio' => 'Поле ФИО должно содержать 3 слова',
        'isEmail' => 'Неверный формат электронной почты',
        'isDate' => 'Неверный формат даты',
    ];

    public function validate($post_array) {
        foreach (array_keys($post_array) as $field) {
            if (empty($this->rules[$field]))
                continue;
            $current_rules = explode('|', $this->rules[$field]);
            foreach ($current_rules as $rule) {
                $explode_result = explode(':', $rule);
                $func = $explode_result[0];
                if (count($explode_result) > 1)
                    $args = $explode_result[1];
                $result = FALSE;
                if (isset($args)) {
                    $result = $this->{$func}($post_array[$field],$args);
                }
                else {
                    $result = $this->{$func}($post_array[$field]);
                }              
                if (!$result) 
                {
                    if (empty($this->messages[$func]))
                        $message = '';
                    else
                        $message = $this->messages[$func];
                    if (!empty($this->errors[$field])) 
                    {
                        $this->errors[$field] = $this->errors[$field].'; '.$message;
                    }
                    else 
                    {
                        $this->errors[$field] = $message;
                    }
                }  
            }
        }
    }
    public function setRule($field, $rule) {
        if (!empty($this->rules[$field])) { 
            $this->rules[$field] = $this->rules[$field].'|'.$rule;
        }
        else { 
            $this->rules[$field] = $rule;
        }
    }

    public function showErrors() {
        foreach ($this->errors as $error)
        {
            echo $error;
        }
    }

    public function showError($field) {
        if (!empty($this->errors[$field]))
        {
            echo $this->errors[$field];
        }
    }

    public function isErrorSet($field) {
        return isset($this->errors[$field]);
    }

    public function isNotEmpty($data) {
        return !empty($data);
    }
    public function isInteger($data) {
        return ;
    }
    public function isLess($data, $value) {
        return $data < $value;
    }
    public function isGreater($data, $value) {
        return $data > $value;
    }
    public function isEmail($data) {
        return filter_var($data, FILTER_VALIDATE_EMAIL);
    }
    public function isFio($data) {
        return count(explode(' ', $data)) === 3;
    }
    public function isPhone($data) {
        return preg_match('/^((\+7)|(\+3))[0-9]{8,10}$/', $data);
    }
    public function isDate($data) {
        return preg_match('/^\b(0?[1-9]|1[012])([\/\-])(0?[1-9]|[12]\d|3[01])\2(\d{4})$/', $data);
    }
}