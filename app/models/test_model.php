<?php
require __DIR__.'/../core/model.php';
require __DIR__.'/validators/custom_form_validation.php';
require __DIR__.'/validators/results_verification.php';
class TestModel extends Model
{
    public $name = '';
    public $group = '';
    public $ans_1_1 = '';
    public $ans_1_2 = '';
    public $ans_2 = '';
    public $ans_3 = '';
    public $testResults = [];
    function __construct()
    {
        parent::__construct();

        if ($_SERVER["REQUEST_METHOD"] != "POST") return;

        $this->validator = new ResultsVerification();
        $this->validator->SetRule('name', 'isNotEmpty');
        $this->validator->SetRule('name', 'isFio');
        $this->validator->SetRule('group', 'isNotEmpty');
        $this->validator->SetRule('ans_3', 'isNotEmpty');
        $this->validator->SetRule('ans_3', 'isOneLetter');
        $this->validator->SetRule('ans_1_1', 'testAnswer1:'.$this->ans_1_2);
        $this->validator->setRule('ans_2', 'testAnswer2');
        $this->validator->setRule('ans_3', 'testAnswer3');
        $this->validator->validate([
            'name' => $this->name,
            'group' => $this->group,
            'ans_1_1' => $this->ans_1_1,
            'ans_2' => $this->ans_2,
            'ans_3' => $this->ans_3,
        ]);
    }

    public function testResult($data) {
        return empty($this->validator->errors[$data]);
    }
}