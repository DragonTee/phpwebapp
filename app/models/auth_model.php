<?php
require __DIR__.'/../core/model.php';
class AuthModel extends Model
{
    public $fio = "";
    public $email = "";
    public $login = "";
    public $password = "";
    function __construct()
    {
        parent::__construct();
        
        if ($_SERVER["REQUEST_METHOD"] != "POST") return;

        $this->validator->SetRule('fio', 'isNotEmpty');
        $this->validator->SetRule('fio', 'isFio');
        $this->validator->SetRule('email', 'isNotEmpty');
        $this->validator->SetRule('email', 'isEmail');
        $this->validator->SetRule('login', 'isNotEmpty');
        $this->validator->SetRule('password', 'isNotEmpty');
        $this->validator->validate([
            'fio' => $this->fio,
            'password' => $this->password,
            'email' => $this->email,
            'login' => $this->login,
        ]);
    }
}