const photoLinks = [
    'https://picsum.photos/id/500/800',
    'https://picsum.photos/id/501/800',
    'https://picsum.photos/id/502/800',
    'https://picsum.photos/id/503/800',
    'https://picsum.photos/id/504/800',
    'https://picsum.photos/id/505/800',
    'https://picsum.photos/id/506/800',
    'https://picsum.photos/id/507/800',
    'https://picsum.photos/id/508/800',
    'https://picsum.photos/id/509/800',
    'https://picsum.photos/id/510/800',
    'https://picsum.photos/id/511/800',
    'https://picsum.photos/id/512/800',
    'https://picsum.photos/id/513/800',
    'https://picsum.photos/id/514/800'
]

const photoTitles = [
    'Фото 1',
    'Фото 2',
    'Фото 3',
    'Фото 4',
    'Фото 5',
    'Фото 6',
    'Фото 7',
    'Фото 8',
    'Фото 9',
    'Фото 10',
    'Фото 11',
    'Фото 12',
    'Фото 13',
    'Фото 14',
    'Фото 15'
]

const maxPhotos = 15;
const columns = 3;

function getPhotos(n) {
    if (n >= maxPhotos) {
        n = maxPhotos;
    }
    for (var i = 0; i < n; i++) {
        if (i % columns == 0) {
            document.write(`<div class="row">`);
        }
        document.write(`<div class="col">`);
        document.write(`<img class="popupable" src="${photoLinks[i]}" title="${photoTitles[i]}" alt = "${photoTitles[i]}">`);
        document.write(`<p class="centered">${photoTitles[i]}</p>`)
        document.write(`</div>`);
        if (i % columns == columns - 1) {
            document.write(`</div>`);
        }
    }
    setModal();
}

$(function() {
    setModal();
});

function setModal() {
    $(".popupable").click(function() { setImage(this) });
    $("#modal-close").click(function() {
        $("#modal-main").hide();
    });
}

function setImage(img) {
    $("#modal-main").show();
    var id = photoLinks.indexOf(img.src);
    $("#modal-image").attr("src", photoLinks[id]);
    $("#modal-caption").text(photoTitles[id]);
}

function slideImage(direction) {
    var id = photoLinks.indexOf($("#modal-image").attr("src"));
    id = (id + direction + photoLinks.length) % photoLinks.length;
    $("#modal-left, #modal-right").hide();
    $("#modal-image").fadeOut(200, function() {
            $("#modal-image").attr('src', photoLinks[id]);
            $("#modal-left, #modal-right").show();
        })
        .fadeIn(100);
    $("#modal-caption").fadeOut(200, function() {
            $("#modal-caption").text(photoTitles[id]);
        })
        .fadeIn(100);
}