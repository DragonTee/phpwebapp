var monthSelect;
var yearSelect;
var daysParent;
var calendarTarget;

function calendarDropdown(event) {
    $("#calendarDropdown").addClass("show");
    calendarTarget = event.target;
}

window.onclick = function(event) {
    if (!event.target.matches('.calendar-field')) {
        var dropdowns = document.getElementsByClassName("dropdown-content");
        var i;
        for (i = 0; i < dropdowns.length; i++) {
            var openDropdown = dropdowns[i];
            if (openDropdown.classList.contains('show')) {
                openDropdown.classList.remove('show');
            }
        }
    }
}

function initCalendar() {
    monthSelect = document.getElementById("calendar-month");
    yearSelect = document.getElementById("calendar-year");
    daysParent = document.getElementsByClassName("calendar-table")[0];
    monthSelect.onchange = updateDays;
    yearSelect.onchange = updateDays;
    updateDays();
}

function updateDays() {
    var days = daysInMonth(+monthSelect.value + 1, +yearSelect.value);
    var resultHTML =
        `<div class="calendar-field calendar-cell-empty">
        <h4>M</h4>
    </div>
    <div class="calendar-field calendar-cell-empty">
        <h4>T</h4>
    </div>
    <div class="calendar-field calendar-cell-empty">
        <h4>W</h4>
    </div>
    <div class="calendar-field calendar-cell-empty">
        <h4>T</h4>
    </div>
    <div class="calendar-field calendar-cell-empty">
        <h4>F</h4>
    </div>
    <div class="calendar-field calendar-cell-empty">
        <h4>S</h4>
    </div>
    <div class="calendar-field calendar-cell-empty">
        <h4>S</h4>
    </div>`;
    for (var day = -dayOfTheWeek(+monthSelect.value + 1, +yearSelect.value); day < days; day++) {
        if (day < 0) {
            resultHTML +=
                `<div class="calendar-field calendar-cell-empty">
            </div>`
            continue;
        }
        resultHTML +=
            `<div class="calendar-field calendar-cell" onclick="setDateField(${day+1})">
            <h4>${day+1}</h4>
        </div>`
    }
    daysParent.innerHTML = resultHTML;
}

function daysInMonth(month, year) {
    return new Date(year, month, 0).getDate();
}

function dayOfTheWeek(month, year) {
    return new Date(year, month - 1, 0).getDay();
}

function setDateField(day) {
    calendarTarget.value = `${pad(+monthSelect.value + 1, 2)}/${pad(day, 2)}/${yearSelect.value}`;
    checkDate(calendarTarget);
}