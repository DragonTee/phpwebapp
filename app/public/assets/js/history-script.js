const pages = [
    "Главная",
    "Обо мне",
    "Мои интересы",
    "Учеба",
    "Фотоальбом",
    "Контакт",
    "Тест по дисциплине",
    "История просмотра"
]

function getHistory() {
    for (var i = 0; i < 8; i++) {
        var history = getPageHistory(i);
        document.write("<tr>");
        document.write(`<td>${pages[i]}</td>`);
        document.write(`<td>${history[0]}</td>`);
        document.write(`<td>${history[1]}</td>`);
        document.write("</tr>");
    }

}

function getPageHistory(id) {
    var localCount = window.sessionStorage.getItem(id);
    if (localCount == null)
        localCount = 0;
    var key = id + '=';
    var value = 0
    var cookie = document.cookie;
    if (cookie.indexOf(key) >= 0) {
        value = cookie.substring(cookie.indexOf(key) + key.length).split(";")[0];
    }
    return [localCount, value];
}

function incHistory(id) {
    var localCount = window.sessionStorage.getItem(id);
    if (localCount == null)
        localCount = 0;
    window.sessionStorage.setItem(id, +localCount + 1);
    var cookie = document.cookie;
    var key = id + '=';
    var value = 0
    if (cookie.indexOf(key) >= 0) {
        value = cookie.substring(cookie.indexOf(key) + key.length).split(";")[0];
    }
    value = +value + 1;
    document.cookie = key + value;
}