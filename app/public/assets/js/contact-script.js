function checkContactForm(form) {
    /*var inputs = form.getElementsByTagName('input');
    var textareas = form.getElementsByTagName('textarea');
    for (var i = 0; i < inputs.length; i++) {
        if (inputs[i].value == "") {
            alert("Не все поля были заполнены");
            inputs[i].focus();
            return false;
        }
    }
    for (i = 0; i < textareas.length; i++) {
        if (textareas[i].value == "") {
            alert("Не все поля были заполнены");
            textareas[i].focus();
            return false;
        }
    }
    var nameField = document.getElementById('name');
    const nameRegex = /^(?:[A-Za-zА-Яа-я]+ ){2}[A-Za-zА-Яа-я]+$/;
    if (!(nameRegex.test(nameField.value))) {
        alert("Поле ФИО должно содержать 3 слова, разделенных пробелами");
        nameField.focus();
        return false;
    }
    var phoneField = document.getElementById('phone');
    const phoneRegex = /^((\+7)|(\+3))[0-9]{8,10}$/;
    if (!(phoneRegex.test(phoneField.value))) {
        alert("Поле телефон должно начинаться с +7 или +3, не должно содержать пробелов и должно содержать от 9 до 11 цифр");
        nameField.focus();
        return false;
    }*/
    return true;
}

function checkName(element) {
    var name = element.value;
    var errorElement = document.getElementById("name-error");
    const nameRegex = /^(?:[A-Za-zА-Яа-я]+ ){2}[A-Za-zА-Яа-я]+$/;
    if (nameRegex.test(name)) {
        if (!element.classList.contains("valid-input"))
            element.classList.add("valid-input")
        element.classList.remove("invalid-input")
        errorElement.innerText = "";
    } else {
        if (!element.classList.contains("invalid-input"))
            element.classList.add("invalid-input")
        element.classList.remove("valid-input")
        errorElement.innerText = "Поле ФИО должно содержать 3 слова, разделенных пробелами";
    }
    checkAllFields();
}

function checkPhone(element) {
    var phone = element.value;
    var errorElement = document.getElementById("phone-error");
    const phoneRegex = /^((\+7)|(\+3))[0-9]{8,10}$/;
    if (phoneRegex.test(phone)) {
        if (!element.classList.contains("valid-input"))
            element.classList.add("valid-input")
        element.classList.remove("invalid-input")
        errorElement.innerText = "";
    } else {
        if (!element.classList.contains("invalid-input"))
            element.classList.add("invalid-input")
        element.classList.remove("valid-input")
        errorElement.innerText = "Поле телефон должно начинаться с +7 или +3, не должно содержать пробелов и должно содержать от 9 до 11 цифр";
    }
    checkAllFields();
}

function checkEmail(element) {
    var email = element.value;
    var errorElement = document.getElementById("email-error");
    const emailRegex = /(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9]))\.){3}(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/
    if (emailRegex.test(email)) {
        if (!element.classList.contains("valid-input"))
            element.classList.add("valid-input")
        element.classList.remove("invalid-input")
        errorElement.innerText = "";
    } else {
        if (!element.classList.contains("invalid-input"))
            element.classList.add("invalid-input")
        element.classList.remove("valid-input")
        errorElement.innerText = "Неверный формат email";
    }
    checkAllFields();
}

function checkDate(element) {
    var date = element.value;
    var errorElement = document.getElementById("date-error");
    const dateRegex = /^\b(0?[1-9]|1[012])([\/\-])(0?[1-9]|[12]\d|3[01])\2(\d{4})$/;
    if (dateRegex.test(date)) {
        if (!element.classList.contains("valid-input"))
            element.classList.add("valid-input")
        element.classList.remove("invalid-input")
        errorElement.innerText = "";
    } else {
        if (!element.classList.contains("invalid-input"))
            element.classList.add("invalid-input")
        element.classList.remove("valid-input")
        errorElement.innerText = "Дата должна быть введена в формате ММ/ЧЧ/ГГГГ";
    }
    checkAllFields();
}

function checkMessage(element) {
    var value = element.value;
    var errorElement = document.getElementById("message-error");
    if (value != "") {
        element.classList.add("valid-input")
        element.classList.remove("invalid-input")
        errorElement.innerText = "";
    } else {
        if (!element.classList.contains("invalid-input"))
            element.classList.add("invalid-input")
        element.classList.remove("valid-input")
        errorElement.innerText = "Сообщение должно быть заполнено";
    }
    checkAllFields();
}

function checkAllFields() {

}

function clearAllFields() {
    var errors = document.getElementsByClassName("error-block");
    [].forEach.call(errors, function(er) { er.innerText = ""; });
    var vinputs = document.getElementsByClassName("valid-input");
    var ivinputs = document.getElementsByClassName("invalid-input");
    for (var i = vinputs.length - 1; i >= 0; i--) {
        vinputs[i].classList.remove("valid-input");
    }
    for (i = ivinputs.length - 1; i >= 0; i--) {
        ivinputs[i].classList.remove("invalid-input");
    }
    checkAllFields();
}

function showPopover(element) {
    $(element).prev().stop(true, true).show();
}

function hidePopover(element) {
    $(element).prev().delay(1000).fadeOut(200);
}

function showModalPrompt() {
    $("#modal-prompt").show();
    $("header, section").addClass("blur-all");
}

function hideModalPrompt() {
    $("#modal-prompt").hide();
    $("header, section").removeClass("blur-all");
}

function submitForm() {
    $("#contact-form").submit();
}