function hoverNavbarIcon(event) {
    var parent = event.target;
    var element = parent.getElementsByTagName('img')[0];
    element['src'] = "https://picsum.photos/id/132/100";
}

function leaveNavbarIcon(event) {
    var parent = event.target;
    var element = parent.getElementsByTagName('img')[0];
    element['src'] = "https://picsum.photos/id/130/100";
}

function interestsDropdown(event) {
    var element = document.getElementById("interestsDropdown");
    element.classList.toggle("show");
}

function guestsDropdown(event) {
    var element = document.getElementById("guestsDropdown");
    element.classList.toggle("show");
}

function blogDropdown(event) {
    var element = document.getElementById("blogDropdown");
    element.classList.toggle("show");
}

window.onclick = function(event) {
    if (!event.target.matches('.dropbtn')) {
        var dropdowns = document.getElementsByClassName("dropdown-content");
        var i;
        for (i = 0; i < dropdowns.length; i++) {
            var openDropdown = dropdowns[i];
            if (openDropdown.classList.contains('show')) {
                openDropdown.classList.remove('show');
            }
        }
    }
}

window.onload = setInterval(clockUpdate, 1000);

const days = [
    'Воскресенье',
    'Понедельник',
    'Вторник',
    'Среда',
    'Четверг',
    'Пятница',
    'Суббота'
]

function clockUpdate() {
    var clock = document.getElementById("clockText");
    var today = new Date();
    var day = pad(today.getDate(), 2);
    var month = pad(today.getMonth() + 1, 2);
    var year = pad(today.getFullYear(), 2);
    var hour = pad(today.getHours(), 2);
    var minutes = pad(today.getMinutes(), 2);
    var seconds = pad(today.getSeconds(), 2);
    clock.innerHTML = `<div>${day}.${month}.${year} ${days[today.getDay()]}</div><div>${hour}:${minutes}:${seconds}</div>`
}

function pad(num, size) {
    var s = "000000000" + num;
    return s.substr(s.length - size);
}