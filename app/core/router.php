<?php
class Router
{
    static function route()
    {
        session_start();

        if($_REQUEST['admin_area']){
            $admin_path = 'admin/';
            $admin_file_prefix = 'admin_';
            $admin_class_prefix = 'Admin';
        } 
        else
        {
            $admin_path = '';
            $admin_file_prefix = '';
            $admin_class_prefix = '';
        }

        $controller_name = $_REQUEST["controller"] ? $_REQUEST["controller"] : "home";

        $action_name = $_REQUEST['action'] ? $_REQUEST['action'] : "index";

        $controller_file = "app/${admin_path}controllers/".$admin_file_prefix.$controller_name.'_controller.php';

        if(file_exists($controller_file)){
            include $controller_file;
        } else{
            die("Controller $controller_file not found!");
        }

        $controller_class_name = $admin_class_prefix.ucfirst($controller_name).'Controller';
        $controller = new $controller_class_name;

        $model_name = $controller_name.'_model';
        $model_file = "app/models/".$model_name.'.php';

        if(file_exists($model_file)) {
            include $model_file;
        } else {
            die("Model $model_file not found");
        }

        $model_class_name = ucfirst($controller_name).'Model';
        $model = new $model_class_name;

        $controller->model = $model;

        if(method_exists($controller, $action_name)) {
            $controller->$action_name();            
        } else {
            die("Method $action_name of controller $controller_class_name not found");
        }
        static::saveStats($_SERVER['REQUEST_URI']);
    }

    private static function saveStats($page)
    {
        if (!class_exists('StatsRecord'))
            require __DIR__.'/../entities/stats.php';
        $record = new StatsRecord();
        $record->saveStats($page);
    }
}