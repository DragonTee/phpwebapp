<?php
require __DIR__.'/../models/validators/form_validation.php';
class Model
{
    protected $validator;
    public $validator_class;
    function __construct()
    {
        $this->validator = new FormValidation();
        if ($_SERVER["REQUEST_METHOD"] == "POST") { 
            foreach (array_keys(get_object_vars($this)) as $field)
            {
                if (!empty($_POST[$field]))
                {
                    $this->$field = $_POST[$field];
                }
            }
        }
    }
    function validate($post_data)
    {
        $this->validator->validate($post_data);
    }
    function showErrors()
    {
        $this->validator->showErrors();
    }
    function showError($field)
    {
        $this->validator->showError($field);
    }
    public function isErrorSet($field) {
        return $this->validator->isErrorSet($field);
    }
    function countErrors()
    {
        return count($this->validator->errors);
    }
}